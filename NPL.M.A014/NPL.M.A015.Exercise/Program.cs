﻿// See https://aka.ms/new-console-template for more information
using NPL.M.A015.Exercise;

using System.Collections;
using System.Text.RegularExpressions;

internal class Program
{

    public static void Menu()
    {
        Console.WriteLine("=== MENU ===");
        Console.WriteLine("1. Get Departments by Number of Employees");
        Console.WriteLine("2. Get Working Employees");
        Console.WriteLine("3. Get Employees by Language");
        Console.WriteLine("4. Get Languages by Employee ID");
        Console.WriteLine("5. Get Senior Employees");
        Console.WriteLine("6. Get Employees with Paging");
        Console.WriteLine("7. Get Departments with Employees");
        Console.WriteLine("8. Input Employee");
        Console.WriteLine("9. Input Department");
        Console.WriteLine("10. Input Language");
        Console.WriteLine("11. Input Employee Language");
        Console.WriteLine("12. Quit");
        Console.WriteLine();
    }
    private static void Main(string[] args)
    {
        Regex rgInt = new Regex("[0-9]");
        Quanli manage = new Quanli();
        string choice, input1;
        do
        {
            Menu();
            do
            {
                Console.Write("Enter your choice: ");
                choice = Console.ReadLine();

            } while (!rgInt.IsMatch(choice));

            switch (choice)
            {
                case "1":
                    do
                    {
                        Console.Write("Enter number of Employee: ");
                        input1 = Console.ReadLine();
                    } while (!rgInt.IsMatch(input1));
                    Console.WriteLine("==============Department==============");
                    foreach (Department d in manage.GetDepartments(Convert.ToInt32(input1)))
                    {
                        Console.WriteLine(d.Id + "  |" + d.Name + "  |");
                    }
                    break;

                case "2":
                    Console.WriteLine("=========Employee Working==========");
                    foreach (Employee employee in manage.GetEmployeesWorking())
                    {
                        Console.WriteLine($"{employee.EmployeeId,-5} | {employee.EmployeeName,-20} | {employee.Age,-5} | {employee.Address,-30} | {employee.DeparmentID}");
                    }
                    break;

                case "3":
                    Console.Write("Enter Language:  ");
                    input1 = Console.ReadLine();
                    foreach (Employee employee in manage.GetEmployees(input1))
                    {
                        Console.WriteLine($"{employee.EmployeeId,-5} | {employee.EmployeeName,-20} | {employee.Age,-5} | {employee.Address,-30} | {employee.DeparmentID}");
                    }
                    break;

                case "4":
                    do
                    {
                        Console.Write("Enter Employee ID: ");
                        input1 = Console.ReadLine();
                    } while (!rgInt.IsMatch(input1));
                    Console.Write("Language: ");
                    Console.WriteLine(manage.GetLanguages(Convert.ToInt32(input1)));
                    break;

                case "5":
                    Console.WriteLine("Senior: ");
                    foreach (Employee employee in manage.GetSeniorEmployees())
                    {
                        Console.WriteLine($"{employee.EmployeeId,-5} | {employee.EmployeeName,-20} | {employee.Age,-5} | {employee.Address,-30} | {employee.DeparmentID}");
                    }
                    break;

                case "6":
                    string pageIndex, pageSize, employeeName, order;
                    int pageind = 1;
                    int pagesi = 10;
                    Console.Write("Enter index: ");
                    pageIndex = Console.ReadLine();
                    try
                    {
                        pageind = Convert.ToInt32(pageIndex);
                    }
                    catch (Exception)
                    {
                        pageind = 0;
                    }

                    Console.Write("Enter size: ");
                    pageSize = Console.ReadLine();
                    try
                    {
                        pagesi = Convert.ToInt32(pageSize);
                    }
                    catch (Exception)
                    {
                        pagesi = 10;
                    }
                    Console.Write("Enter Name: ");
                    employeeName = Console.ReadLine();
                    Console.Write("Enter order of Name(ASC/DESC): ");
                    order = Console.ReadLine();
                    List<Employee> employees = manage.GetEmployeePaging(pageind, pagesi, employeeName, order);

                    if (employees.Count > 0)
                    {
                        Console.WriteLine("Employees:");

                        foreach (var employee in employees)
                        {
                            Console.WriteLine($"{employee.EmployeeId,-5}  |  {employee.EmployeeName,-20} | {employee.Age,-5} | {employee.Address,-30} | {employee.DeparmentID}");
                        }
                    }
                    else
                    {
                        Console.WriteLine("No employees found.");
                    }
                    break;

                case "7":
                    Console.WriteLine("==================Department and Employee================");
                    foreach (DictionaryEntry entry in manage.GetDepartments())
                    {
                        var department = (Department)entry.Key;
                        var employeesInDepartment = (List<Employee>)entry.Value;

                        Console.WriteLine($"Department: {department.Name}");
                        Console.WriteLine("Employees: ");

                        foreach (var employee in employeesInDepartment)
                        {
                            Console.WriteLine($"{employee.EmployeeId,-5}  |  {employee.EmployeeName,-20} | {employee.Age,-5} | {employee.Address,-30} | {employee.DeparmentID}");
                        }

                        Console.WriteLine();
                    }
                    break;
                case "8":
                    manage.InputEmployees();
                    break;
                case "9":
                    manage.InputDepartments();
                    break;
                case "10":
                    manage.InputProgrammingLanguages();
                    break;
                case "11":
                    manage.InputEmployeeSkills();
                    break;
                default:
                    Console.WriteLine("Invalid choice. Please try again.");
                    break;

            }
        } while (true);
    }
}
