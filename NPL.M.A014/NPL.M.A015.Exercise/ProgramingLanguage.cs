﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class ProgramingLanguage
    {
        public ProgramingLanguage()
        {
        }

        public string LanguageName { get; set; }

        public string LanguageId { get; set; }

        public ProgramingLanguage(string languageName, string languageId)
        {
            LanguageName = languageName;
            LanguageId = languageId;
        }
    }
}
