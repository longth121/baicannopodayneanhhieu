﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Quanli
    {
        List<Employee> employees = new List<Employee>()
        {
            
            new Employee { EmployeeId = 1, EmployeeName = "Nguyễn Văn A", Age = 20, Address = "Hà Nội", HireDate = new DateOnly(2023, 10, 16), Status = true },
            new Employee { EmployeeId = 2, EmployeeName = "Trần Thị B", Age = 22, Address = "HCM", HireDate = new DateOnly(2023, 08, 26), Status = true },
            new Employee { EmployeeId = 3, EmployeeName = "Lê Văn C", Age = 21, Address = "Hải Phòng", HireDate = new DateOnly(2023, 09, 26), Status = true },
            new Employee { EmployeeId = 4, EmployeeName = "Phạm Văn D", Age = 23, Address = "Vinh", HireDate = new DateOnly(2023, 01, 26), Status = true },
            new Employee { EmployeeId = 5, EmployeeName = "Hoàng Thị E", Age = 24, Address = "Đà Nẵng", HireDate = new DateOnly(2022, 10, 26), Status = false }
        };

        List<ProgramingLanguage> programingLanguages = new List<ProgramingLanguage>()
        {
            new ProgramingLanguage {LanguageName = "C#"},
            new ProgramingLanguage {LanguageName = "Java"},
            new ProgramingLanguage {LanguageName = "Python"}
        };

        List<Department> department = new List<Department>()
        {
            new Department {DepartmentId = 1, DepartmentName = "Backend"},
            new Department {DepartmentId = 2, DepartmentName = "FullStack"},
            new Department {DepartmentId = 3, DepartmentName = "Frontend" }
        };

        public List<Employee> GetEmployeesWorking(List<Employee> employees)
        {
            var emp =  employees.Where(employee => employee.Status).ToList();
            return emp;
        }

        List<EmployeeSkill> employeeSkills = new List<EmployeeSkill>
        {
            new EmployeeSkill(1,1),
            new EmployeeSkill(4,1),
            new EmployeeSkill(1,2),
            new EmployeeSkill(1,3),
            new EmployeeSkill(2,3),
        };

        public void InputEmployees()
        {
            Console.WriteLine("=== Input Employees ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Employee ID: ");
                string idInput = Console.ReadLine();

                if (!int.TryParse(idInput, out int id))
                {
                    Console.WriteLine("Invalid Employee ID. Please try again.");
                    continue;
                }

                // Check if ID already exists
                if (employees.Any(e => e.EmployeeId == id))
                {
                    Console.WriteLine("Employee ID already exists. Please enter a different ID.");
                    continue;
                }

                Console.Write("Employee Name: ");
                string name = Console.ReadLine();

                Console.Write("Department ID: ");
                string deptIdInput = Console.ReadLine();

                if (!int.TryParse(deptIdInput, out int deptId))
                {
                    Console.WriteLine("Invalid Department ID. Please try again.");
                    continue;
                }

                Console.Write("Age: ");
                string ageInput = Console.ReadLine();

                if (!int.TryParse(ageInput, out int age))
                {
                    Console.WriteLine("Invalid Age. Please try again.");
                    continue;
                }

                Console.Write("Address: ");
                string address = Console.ReadLine();

                Console.Write("Hired Date (yyyy-MM-dd): ");
                string hiredDateInput = Console.ReadLine();

                if (!DateOnly.TryParse(hiredDateInput, out DateOnly hiredDate))
                {
                    Console.WriteLine("Invalid Hired Date. Please try again.");
                    continue;
                }

                Console.Write("Status (true/false): ");
                string statusInput = Console.ReadLine();

                if (!bool.TryParse(statusInput, out bool status))
                {
                    Console.WriteLine("Invalid Status. Please try again.");
                    continue;
                }

                Employee employee = new Employee(id, name, deptId, age, address, hiredDate, status);

                employees.Add(employee);

                Console.Write("Do you want to continue inputting employees? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }

        public void InputDepartments()
        {
            Console.WriteLine("=== Input Departments ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Department ID: ");
                string idInput = Console.ReadLine();

                if (!int.TryParse(idInput, out int id))
                {
                    Console.WriteLine("Invalid Department ID. Please try again.");
                    continue;
                }

                // Check if ID already exists
                if (department.Any(d => d.Id == id))
                {
                    Console.WriteLine("Department ID already exists. Please enter a different ID.");
                    continue;
                }

                Console.Write("Department Name: ");
                string name = Console.ReadLine();

                Department departments = new Department(id, name);

                department.Add(departments);

                Console.Write("Do you want to continue inputting departments? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }

        public void InputEmployeeSkills()
        {
            Console.WriteLine("=== Input Employee Skills ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Employee ID: ");
                string empIdInput = Console.ReadLine();

                if (!int.TryParse(empIdInput, out int empId))
                {
                    Console.WriteLine("Invalid Employee ID. Please try again.");
                    continue;
                }

                if (!employees.Any(e => e.EmployeeId == empId))
                {
                    Console.WriteLine("Employee ID does not exist. Please enter a valid Employee ID.");
                    continue;
                }

                Console.Write("Language ID: ");
                string langIdInput = Console.ReadLine();

                if (!int.TryParse(langIdInput, out int langId))
                {
                    Console.WriteLine("Invalid Language ID. Please try again.");
                    continue;
                }

                if (!programingLanguages.Any(l => l.LanguageId.Equals(langId)))
                {
                    Console.WriteLine("Language ID does not exist. Please enter a valid Language ID.");
                    continue;
                }

                if (employeeSkills.Any(es => es.EmpID == empId && es.LangID == langId))
                {
                    Console.WriteLine("Employee already has this Language skill. Please enter a different Language ID.");
                    continue;
                }

                EmployeeSkill employeeSkill = new EmployeeSkill(empId, langId);

                employeeSkills.Add(employeeSkill);

                Console.Write("Do you want to continue inputting employee skills? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }
        public void InputProgrammingLanguages()
        {
            Console.WriteLine("=== Input Programming Languages ===");

            bool continueInput = true;

            while (continueInput)
            {
                Console.Write("Language ID: ");
                string idInput = Console.ReadLine();

                if (!int.TryParse(idInput, out int id))
                {
                    Console.WriteLine("Invalid Language ID. Please try again.");
                    continue;
                }

                if (programingLanguages.Any(pl => pl.LanguageId.Equals(id)))
                {
                    Console.WriteLine("Language ID already exists. Please enter a different ID.");
                    continue;
                }

                Console.Write("Language Name: ");
                string name = Console.ReadLine();

                ProgramingLanguage programmingLanguage = new ProgramingLanguage(idInput, name);

                programingLanguages.Add(programmingLanguage);

                Console.Write("Do you want to continue inputting programming languages? (Y/N): ");
                string continueInputStr = Console.ReadLine();

                continueInput = (continueInputStr.ToUpper() == "Y");
            }
        }
        public List<Department> GetDepartments(int numberOfEmployees)
        {
            var departmentsWithEmployees = (from dp in department
                                            join ep in employees on dp.Id equals ep.EmployeeId
                                            group ep by new { dp.Id, dp.Name } into g
                                            where g.Count() > numberOfEmployees
                                            select new Department
                                            {
                                                Id = g.Key.Id,
                                                Name = g.Key.Name,
                                            }).ToList();

            return departmentsWithEmployees;
        }
        public List<Employee> GetEmployeesWorking()
        {
            var workingEmployees = employees.Where(e => e.Status).ToList();
            return workingEmployees;
        }
        public List<Employee> GetEmployees(string languageName)
        {
            var employeesWithLanguage = (from emp in employees
                                        join empSkill in employeeSkills on emp.EmployeeId equals empSkill.EmpID
            //                             join lang in programingLanguages on empSkill.LangID equals lang.LanguageId
            //                             where lang.Name == languageName
                                         select emp).Distinct().ToList();
            return employeesWithLanguage;
        }
        public List<Employee> GetSeniorEmployees()
        {
            var seniorEmployees = (from emp in employees
                                   join empSkill in employeeSkills on emp.EmployeeId equals empSkill.EmpID
                                   group emp by emp.EmployeeId into g
                                   where g.Count() > 1
                                   select g.Key).ToList();

            var seniorEmployeesList = employees.Where(e => seniorEmployees.Contains(e.EmployeeId)).ToList();
            return seniorEmployeesList;
        }
        public List<Employee> GetEmployeePaging(int pageIndex = 1, int pageSize = 10, string employeeName = null, string order = "ASC")
        {
            var filteredEmployees = employees;
            if (!string.IsNullOrEmpty(employeeName))
            {
                filteredEmployees = filteredEmployees.Where(e => e.EmployeeName.Contains(employeeName)).ToList();
            }

            if (order == "ASC")
            {
                filteredEmployees = filteredEmployees.OrderBy(e => e.EmployeeName).ToList();
            }
            else if (order == "DESC")
            {
                filteredEmployees = filteredEmployees.OrderByDescending(e => e.EmployeeName).ToList();
            }

            int startIndex = (pageIndex - 1) * pageSize;

            var pagedEmployees = filteredEmployees.Skip(startIndex).Take(pageSize).ToList();

            return pagedEmployees;
        }
        public string GetLanguages(int employeeId)
        {
            string lang = "";
            foreach (EmployeeSkill employeeSkill in employeeSkills)
            {
                if (employeeSkill.EmpID == employeeId)
                {
                    foreach (ProgramingLanguage language in programingLanguages)
                    {
                        if (language.LanguageId.Equals(employeeSkill.LangID))
                        {
                            lang += language.LanguageName + " ";
                        }
                    }
                }
            }

            return lang;
        }
        public Hashtable GetDepartments()
        {
            var departmentHashtable = new Hashtable();

            foreach (var dept in department)
            {
                var employeesInDepartment = employees.Where(emp => emp.DeparmentID == dept.Id).ToList();
                departmentHashtable.Add(dept, employeesInDepartment);
            }

            return departmentHashtable;
        }
    }

}

