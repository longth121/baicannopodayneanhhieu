﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.M.A015.Exercise
{
    internal class Employee
    {
        public int EmployeeId { get; set; }
        public string? EmployeeName { get; set; }
        public int DeparmentID { get; set; }
        public int Age { get; set; }
        public string? Address { get; set; }
        public DateOnly HireDate {  get; set; } 
        public Boolean Status { get; set; }

        public Employee(int employeeId, string? employeeName, int deparmentID, int age, string? address, DateOnly hireDate, bool status)
        {
            EmployeeId = employeeId;
            EmployeeName = employeeName;
            DeparmentID = deparmentID;
            Age = age;
            Address = address;
            HireDate = hireDate;
            Status = status;
        }

        public Employee() { }

    }
}
