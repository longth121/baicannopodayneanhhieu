﻿using NPL.Practice.T02.Problem03;
using System.Text.RegularExpressions;

internal class Program
{
    private static void Main(string[] args)
    {
        Console.WriteLine("Student grade management");
        List<Student> students = new List<Student>()
        {
            new Student(1, "Long", DateTime.Parse("01/05/2003"), 10, 10, 10, 10)
        };
        Manage manager = new(students);
        int choice;

        do
        {
            Console.WriteLine("Menu options:");
            Console.WriteLine("1. Display students list");
            Console.WriteLine("2. Add students to list");
            Console.WriteLine("0. Exit program");
            choice = Validate.GetUserChoiceAsIntNumber(0, 5, "Select an option 0->2: ");
            switch (choice)
            {
                case 0:
                    Console.WriteLine("Exit program!!!");
                    break;
                case 1:
                    string[] s = { "" };
                    Validate.PrintList<Student>(students, "List of students:",
                        string.Format("{0,-8}", "ID"),
                        string.Format("{0,-32}", "Name"),
                        string.Format("{0,-32}", "StartDate"),
                        string.Format("{0,-8}", "GPA"),
                        string.Format("{0,-20}", "Graduate Level"));
                    break;
                case 2:
                    manager.AddStudentsToList();
                    break;
            }
        } while (choice != 0);
    }
}
