﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Student : IGraduate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public decimal SqlMark { get; set; }
        public decimal CsharpMark { get; set; }
        public decimal DsaMark { get; set; }
        public decimal GPA { get; set; }
        public GraduateLevel GraduateLevel { get; set; }

        public Student()
        {

        }

        public Student(int id, string name, DateTime startDate, decimal sqlMark, decimal csharpMark, decimal dsaMark, decimal gpa)
        {
            Id = id;
            Name = name;
            StartDate = startDate;
            SqlMark = sqlMark;
            CsharpMark = csharpMark;
            DsaMark = dsaMark;
            GPA = gpa;
            // You may set the GraduateLevel as needed based on the GPA.
            CalculateGPA();
        }

        // Method to set GraduateLevel based on GPA
        private void CalculateGPA()
        {
            decimal avg = (this.CsharpMark + this.DsaMark + this.SqlMark) / 3;
            if (avg >= 9)
            {
                GraduateLevel = GraduateLevel.Excellent;
            }
            else if (avg >= 8 && GPA < 9)
            {
                GraduateLevel = GraduateLevel.VeryGood;
            }
            else if (avg >= 7 && GPA < 8)
            {
                GraduateLevel = GraduateLevel.Good;
            }
            else if (avg >= 5 && GPA < 7)
            {
                GraduateLevel = GraduateLevel.Average;
            }
            else
            {
                GraduateLevel = GraduateLevel.Failed;
            }
            GPA = avg;
        }

        public string GetCertificate()
        {
            return "Name: " + this.Name + ", SqlMark: " + this.SqlMark + ", CsharpMark: " + this.CsharpMark + ", DasMark: " + DsaMark + ",Gpa: " + this.GPA + ",Level: " + this.GraduateLevel;
        }

        public override string? ToString()
        {
            return string.Format("{0,-8}{1,-32}{2,-32}{3,-8}{4,-20}",
                                 this.Id,
                                 this.Name,
                                 this.StartDate.ToString("MM/dd/yyyy hh:mm:ss tt"),
                                 Math.Round(this.GPA, 2),
                                 this.GraduateLevel);
        }

        public void Graduate()
        {
            CalculateGPA();
        }
    }

}

