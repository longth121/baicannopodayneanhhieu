﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;

namespace NPL.Practice.T02.Problem03
{
    internal class Validate
    {
        // Input functions
        public static T InputSomethingOfType<T>(string message, string errorMessage, string regex)
        {
            T result = default(T);
            while (true)
            {
                Console.Write(message);
                string tmp = Console.ReadLine();
                if (tmp.Equals(""))
                {
                    Console.WriteLine("This field must not be empty!");
                    continue;
                }
                if (Regex.IsMatch(tmp, regex))
                {
                    try
                    {
                        Console.WriteLine();
                        result = (T)Convert.ChangeType(tmp, typeof(T));
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("Can not parse input to data type " + typeof(T));
                    }
                }
                else
                {
                    Console.WriteLine(errorMessage);
                }
            }
        }
        public static string InputstringOfRegexWithMessage(string message, string error, string regex)
        {
            while (true)
            {
                Console.Write(message);
                string res = Console.ReadLine().Trim();
                if (Regex.IsMatch(res, regex))
                {
                    Console.WriteLine();
                    return res;
                }
                else
                {
                    Console.WriteLine(error);
                }
            }
        }
        public static Boolean PressYNToContinue(string mess)
        {
            Boolean res = "Y".Equals(InputstringOfRegexWithMessage(mess, "Only Y/N accepted!", @"^[ynYN]$").ToUpper());
            return res;
        }
        public static int GetUserChoiceAsIntNumber(int lowerbound, int upperbound, string mess)
        {
            int res;
            while (true)
            {
                //Console.Write(mess);
                res = Convert.ToInt32(InputstringOfRegexWithMessage(mess, "Only digit characters accepted!", @"^\d+$"));
                if (res >= lowerbound && res <= upperbound)
                    return res;
                else
                    Console.WriteLine("Invalid choice! You'll have to input again!");
            }
        }
        public static string getNonEmptyString(string mess)
        {
            string ret = "";
            while (true)
            {
                Console.Write(mess);
                ret = Regex.Replace(Console.ReadLine(), "\\s+", " ");
                if (ret.Equals(""))
                {
                    Console.WriteLine("Please input Non-Empty string!!!");
                    continue;
                }
                return ret;
            }
        }
        // Data handling / manipulating functions
        public static string NormalizeName(string name)
        {
            string res = "";
            string regex = "\\s+";
            string[] strings = name.Trim().Split(regex);
            string[] nameArr = strings;
            for (int i = 0; i < nameArr.Length; i++)
            {
                nameArr[i] = nameArr[i].ToLower();
                res += string.Concat(nameArr[i].Substring(0, 1).ToUpper(), nameArr[i].AsSpan(1));
                if (i < nameArr.Length) res += " ";
            }
            return res;
        }

        // Print result function
        public static void PrintList<T>(List<T> list, string mess, params string[] columnNames)
        {
            Console.WriteLine(mess + "\n");
            foreach (string s in columnNames) Console.Write(s);
            Console.WriteLine();

            foreach (var item in list)
            {
                Console.WriteLine(item.ToString() + "\n");
            }
            Console.WriteLine();
        }
    }
}
