﻿// See https://aka.ms/new-console-template for more information

using System.Net.Sockets;

static string GetArticleSummary(string content, int maxLength)
{
    if (content.Length <= maxLength)
    {
        return content;
    }
    if (content[maxLength - 1] == ' ' ) 
        return content.Substring(0, maxLength-2).Trim() + "...";

    if (content[maxLength - 1] != ' ' && content[maxLength] == ' ' )
        return content.Substring(0, maxLength).Trim() + "...";

    if (content[maxLength - 1] != ' ' && content[maxLength] != ' ')
    {
        for (int i = maxLength -1; i >= 0; i--)
        {
            if (content[i] == ' ') 
            return content.Substring(0, i).Trim() + "...";
        }
    }
    
    
    return "..."; 
}

string contentofarticle;
int maxlenght = 0;
//nhập content
Console.WriteLine("Content: ");
contentofarticle = Console.ReadLine().Trim();

do
{
    try
    {

        Console.WriteLine("MaxLenght: ");
        maxlenght = int.Parse(Console.ReadLine());
        break;
    }
    catch (Exception e) { Console.WriteLine("Chi nhap so"); }

} while (true);


string a = GetArticleSummary(contentofarticle, maxlenght);
Console.WriteLine("Summary: " + a);