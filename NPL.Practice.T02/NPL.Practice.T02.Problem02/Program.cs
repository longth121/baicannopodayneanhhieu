﻿int num = 0;
int subLength = 0;

//nhập số phần tử mảng và giá trị
do
{
    try
    {
        Console.Write("Enter number of element: ");
        num = int.Parse(Console.ReadLine());
        break;
    }catch(Exception) {
        Console.WriteLine("Enter number >0 only!");
    }
} while (num <= 0);

int[] arr = new int[num];

for (int i = 0; i < num; i++)
{
    do
    {
        try
        {
            Console.Write("Enter array[" + i + "] = ");
            arr[i] = int.Parse(Console.ReadLine());
            break;
        }
        catch (Exception)
        {
            Console.WriteLine("Enter number only!");
        }
    } while (true);
}

//nhập số phần tử trong subArray
do
{
    try
    {
        Console.WriteLine("subLength: ");
        subLength = int.Parse(Console.ReadLine());
        break;
    }catch (Exception) { Console.WriteLine("Enter number only!"); }

}while (subLength <= 0);


//truyền mảng và sublength vào hàm tính tổng
int sum = FindMaxSubArray(arr, subLength);

Console.WriteLine(sum);

//hàm tính tổng của subarray
static int FindMaxSubArray(int[] arr, int subLength)
{

    int maxSum = 0; 

    for (int i = 0; i <= arr.Length - subLength; i++)
    {
        int currentSum = 0;
        // tính tổng của các subarray
        for (int j = i; j <i + subLength; j++)
        {
            currentSum += arr[j];
        }

        //gán cho maxsum giá trị lớn nhất
        if (currentSum > maxSum)
        {
            maxSum = currentSum;
        }
    }

    return maxSum;
}
