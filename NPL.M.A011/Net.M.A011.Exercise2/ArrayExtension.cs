﻿public static class ArrayExtensions
{
    public static int[] RemoveDuplicate(this int[] array)
    {
        HashSet<int> uniqueSet = new HashSet<int>(array);
        int[] result = new int[uniqueSet.Count];
        uniqueSet.CopyTo(result);
        return result;
    }

    public static T[] RemoveDuplicate<T>(this T[] array)
    {
        HashSet<T> uniqueSet = new HashSet<T>(array);
        T[] result = new T[uniqueSet.Count];
        uniqueSet.CopyTo(result);
        return result;
    }
}
