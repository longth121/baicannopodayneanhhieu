﻿using System;
using System.Collections;
using System.Linq;

public static class ArrayListExtensions
{
    public static int CountInt(this ArrayList array)
    {
        return array.OfType<int>().Count();
    }

    public static int CountOf(this ArrayList array, Type dataType)
    {
        int count = 0;
        foreach (var item in array)
        {
            if (item.GetType() == dataType)
            {
                count++;
            }
        }
        return count;
    }

    public static int CountOf<T>(this ArrayList array)
    {
        int count = 0;
        foreach (var item in array)
        {
            if (item is T)
            {
                count++;
            }
        }
        return count;
    }
    public static T MaxOf<T>(this ArrayList array) where T : IComparable<T>
    {
        var numericArray = array.OfType<T>().ToList();

        if (numericArray.Count == 0)
        {
            throw new InvalidOperationException("The ArrayList does not contain any elements of type T.");
        }

        return numericArray.Max();
    }

    private static bool IsNumericType<T>()
    {
        return typeof(T).IsPrimitive && typeof(T) != typeof(bool) && typeof(T) != typeof(char);
    }
}
