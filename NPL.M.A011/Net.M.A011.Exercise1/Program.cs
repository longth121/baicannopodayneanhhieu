﻿using System.Collections;

class Program
{
    static void Main(string[] args)
    {
        ArrayList array = new ArrayList { "Hieu", "Hoang", 1, 2, "Trong", 3.9d };

        int countInt = array.CountInt();
        Console.WriteLine("CountInt(): " + countInt);

        int countOfTypeInt = array.CountOf(typeof(int));
        Console.WriteLine("CountOf(typeof(int)): " + countOfTypeInt);

        int countOfTypeString = array.CountOf<string>();
        Console.WriteLine("CountOf<string>(): " + countOfTypeString);

        try
        {
            int maxInt = array.MaxOf<int>();
            Console.WriteLine("MaxOf<int>(): " + maxInt);
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}